'use strict'

const numberOfFilms = +prompt('Сколько фильмов вы уже посмотрели?', '');

let personalMovieDB = {
    count: numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    privat: false
};

if (personalMovieDB.count < 10){
    alert("Просмотрено довольно мало фильмов")
} else if (personalMovieDB.count <= 30) {
    alert("Вы классический зритель");
} else if (personalMovieDB.count > 30) {
    alert("Вы киноман");
} else {
    alert("Произошла ошибка");
}

let movieCounter = 0;


//Через while
while (movieCounter < 2){
    const movie = prompt('Один из последних просмотренных фильмов?', '');
    const movieRating = prompt('На сколько оцените его?', '');
    if ((movie !== null) && (movieRating !== null) && (movie != '') && (movieRating != '') && (movie.length <= 50)){
        personalMovieDB.movies[movie] = +movieRating;
        movieCounter++;
    }
}

//Через for
for (let i = 0; i < 2; i++){
    const movie = prompt('Один из последних просмотренных фильмов?', '');
    const movieRating = prompt('На сколько оцените его?', '');
    if ((movie === null) || (movieRating === null) || (movie == '') || (movieRating == '') || (movie.length > 50)){
        i--;
    } else {
        personalMovieDB.movies[movie] = +movieRating;
    }
}

//Через do...while

movieCounter = 0;

do{
    const movie = prompt('Один из последних просмотренных фильмов?', '');
    const movieRating = prompt('На сколько оцените его?', '');
    if ((movie !== null) && (movieRating !== null) && (movie != '') && (movieRating != '') && (movie.length <= 50)){
        personalMovieDB.movies[movie] = +movieRating;
        movieCounter++;
    }
} while (movieCounter < 2);